﻿using System;
using System.Threading;

namespace Filosofos
{
    public class Palillo
    {
        /*--- Propiedad ---*/
        public int Id { get; set; }

        /*--- Constructor ---*/
        public Palillo(int id)
        {
            Id = id;
        }
    }
    public class Filosofo
    {
        /*--- Propiedade ---*/
        public int Id { get; set; }
        public Palillo PalilloIzquierdo { get; set; }
        public Palillo PalilloDerecho { get; set; }
        static readonly object control = new object();
        /*--- Constructor ---*/
        public Filosofo(int id)
        {
            Id = id;
        }
        /*--- Metodos ---*/
        public void CojerPalilloIzquierdo(Palillo palillo)
        {
            PalilloIzquierdo = palillo;
            Console.WriteLine($"El filosofo {Id} ha cojido el palillo izquierdo {palillo.Id}");
        }
        public void CojerPalilloDerecho(Palillo palillo)
        {
            PalilloDerecho = palillo;
            Console.WriteLine($"El filosofo {Id} ha cojido el palillo dercho {palillo.Id}");
        }
        public void Comer(Palillo palilloIzquierdo, Palillo palilloDerecho)
        {
            if (PalilloIzquierdo == null)
            {
                try
                {
                    CojerPalilloIzquierdo(palilloIzquierdo);
                }
                catch
                {
                    Pensar();
                }
            }
            Thread.Sleep(new Random().Next(1000));
            if (PalilloDerecho == null)
            {
                try
                {
                    CojerPalilloDerecho(palilloDerecho);
                }
                catch
                {
                    Pensar();
                }
            }
            Console.WriteLine($"El filosofo {Id} esta comiendo");
            Thread.Sleep(1000);
        }
        public void Pensar()
        {
            PalilloDerecho = null;
            PalilloIzquierdo = null;
            Console.WriteLine($"El filosofo {Id} esta pensando");
            Thread.Sleep(1000);
        }
        public void Ejecutar(Palillo palilloIzquierdo, Palillo palilloDerecho)
        {
            while (true)
            {
                /*
                * Se que no es lo que se pide pero no he sabido bloquera el recurso
                * asi que e decidido bloquear el codigo, esto lo he sacado de este video:
                * https://www.youtube.com/watch?v=uS9q3_MH-E8
                 */
                lock (control)
                {
                    Comer(palilloIzquierdo, palilloDerecho);
                    Thread.Sleep(1000);
                }
                Pensar();
            }
        }
    }
    internal class Program
    {
        static void Main()
        {
            /*--- Creamaos los palillos ---*/

            Palillo palillo1 = new Palillo(1);
            Palillo palillo2 = new Palillo(2);
            Palillo palillo3 = new Palillo(3);
            Palillo palillo4 = new Palillo(4);
            Palillo palillo5 = new Palillo(5);

            /*--- Creamos los Filosofos ---*/

            Filosofo filosofo1 = new Filosofo(1);
            Filosofo filosofo2 = new Filosofo(2);
            Filosofo filosofo3 = new Filosofo(3);
            Filosofo filosofo4 = new Filosofo(4);
            Filosofo filosofo5 = new Filosofo(5);

            /*--- Creamos los Threads ---*/
            Thread thread1 = new Thread(() =>
            {
                filosofo1.Ejecutar(palillo5, palillo1);
            });
            Thread thread2 = new Thread(() =>
            {
                filosofo2.Ejecutar(palillo1, palillo2);
            });
            Thread thread3 = new Thread(() =>
            {
                filosofo3.Ejecutar(palillo2, palillo3);
            });
            Thread thread4 = new Thread(() =>
            {
                filosofo4.Ejecutar(palillo3, palillo4);
            });
            Thread thread5 = new Thread(() =>
            {
                filosofo5.Ejecutar(palillo4, palillo5);
            });

            /*--- Ejecutamos los threads ---*/
            thread1.Start(); thread2.Start(); thread3.Start(); thread4.Start(); thread5.Start();
        }
    }
}
